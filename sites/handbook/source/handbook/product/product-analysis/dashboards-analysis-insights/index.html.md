---
layout: handbook-page-toc
title: PDI Dashboards, Analysis, & Insights
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is a WIP page to aggregate dashboards, analysis, and insights generated or owned by the 
Product Data Insights team.

### Dashboards

| Dashboard | Description | Date | SAFE Access Required? |
| ------ | ------ |------ |------ |
| [Product Adoption Dashboard](https://app.periscopedata.com/app/gitlab/771580/Product-Adoption-Dashboard)  | Product KPIs | Ongoing | N |
| [Centralized SMAU/GMAU Dashboard](https://app.periscopedata.com/app/gitlab/758607/Centralized-SMAU-GMAU-Dashboard) | Product KPIs | Ongoing | N |
| [Group Namespace Conversion Metrics](https://app.periscopedata.com/app/gitlab:safe-dashboard/919248/Group-Namespace-Conversion-Metrics) | Leading indicators of how new namespaces convert to paid customers | FY21 Q4 | Y |
| [Stages per Organization (SpO) Deep Dive](https://app.periscopedata.com/app/gitlab:safe-dashboard/919345/Stages-per-Organization-Deep-Dive---SpO) | What are the most common stage adoption paths for new group namespaces? How does the adoption of certain stages correlate with long-term engagement, expansion, and conversion? | FY22 Q1 | Y |
| [Cross-Stage Adoption Dashboard](https://app.periscopedata.com/app/gitlab/869174/Cross-Stage-Adoption-Dashboard) | Analyze how organizations engage with different combinations of stages | FY22 | N |
| [Golden Journey Pathways](https://app.periscopedata.com/app/gitlab/897587/Golden-Journey-Paths) | How effectively are namespaces and instances funneling from one stage to another? | FY 22 Q3 | N |
| [Feature Retention](https://app.periscopedata.com/app/gitlab/1003214/Feature-Retention) | Which features continue to be used for months after its initial adoption? Sortable by delivery type (SaaS and Self-Managed) and plan tier | FY23 Q1 | N |
| [Cancellation Reasoning](https://app.periscopedata.com/app/gitlab:safe-dashboard/919393/Cancellation-Reasoning-Dashboard) | Self-reported reasons and verbatims that Saas customers are cancelling in CustomersDot | Ongoing | Y |
| [Trial Feature Adoption](https://app.periscopedata.com/app/gitlab:safe-dashboard/919403/Trial-Feature-Adoption-Dashboard) |  |  | Y |
| [Feature Retention](https://app.periscopedata.com/app/gitlab/1003214/Feature-Retention) |  |  | N |
| [Usage PQL Analysis](https://app.periscopedata.com/app/gitlab:safe-dashboard/926643/Growth:Conversion-Usage-PQL-Analysis) |  |  | Y |


### Analysis & Insights

| Analysis | Description | Date |
| ------ | ------ |------ |
| [Stage Adoption Patterns: SCM <> Code Review <> CI](https://docs.google.com/presentation/d/1BcRhn8kJZTw8QcWSQLAk9mv72lJfk4d2jteGWCBYfo4/edit?usp=sharing) | How do organizations go from using SCM to Verify, and is Code Review used between or after these stages? | FY22 Q1 |
| [Secure Free-to-Paid Funnel and Feature Adoption Analysis](https://docs.google.com/presentation/d/1bbvfsNzKoZw4kCYB9coexiXzPiLZ5E3XPe6hOsbZlag/edit?usp=sharing) | Which Secure features are the most adopted during trials and which features have the highest correlation with Ultimate plan paid conversion? | FY22 Q2 |
| [Paid SpO Trends among Retained and Churned Namespaces](https://docs.google.com/presentation/d/1RR5qwaE2E2OUtfSgU53GMs8FHjexNx2CFJcUbtiNS-0/edit?usp=sharing) | What stage adoption trends are present when looking at churning namespaces vs.retained namespaces? | FY22 Q3 |
